<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>InnoZone</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="innozone-files/style.css">
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.parallax').parallax();
            $('.print').click(function(e) {
                e.preventDefault();
                window.print();
            });

            $('.tg').click(function(ev){
                ev.preventDefault();
                M.toast({html: 'Add @rjordache on your Telegram!'})
            });

            $('.scrollspy').scrollSpy();
            $('.sidenav').sidenav();
        });
  </script>
</head>
<body class="blue-grey darken-2">
    <header class="navbar-fixed">
        <nav class="blue-grey darken-3">
        <div class="nav-wrapper">
            <a href="#intro" class="brand-logo">Rudy Jordache</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="#about">About</a></li>
                <li><a href="#skills">Skills</a></li>
                <li><a href="#contacts">Contacts</a></li>
            </ul>
            <a href="#" data-target="slide-out" class="sidenav-trigger right"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        <ul id="slide-out" class="sidenav">
            <li><a href="#intro">Top</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#skills">Skills</a></li>
            <li><a href="#contacts">Contacts</a></li>
        </ul>
    </header>
    <main>
    
    <div id="intro" class="starter scrollspy">
        <span class="hero">Hi, i'm a FullStack Developer</span>
    </div>

    <div id="about" class="container scrollspy">
        <div class="row">
            <div class="col s12">
                <h2 class="title">About me</h2>
            </div>
            <div class="col s12 m6">
                <picture>
                    <img src="innozone-files/c.png" alt="My life is a FullStack">
                </picture>
            </div>
            <div class="col s12 m6">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <blockquote>
                            <p>Pluri Tecnologia</p>
                            <b>Fullstack web/mobile<b>
                            <br />
                            <small>SEP/2018 at TODAY</small>
                        </blockquote>
                        <blockquote>
                            <p>Paradox Zero</p>
                            <b>Fullstack<b>
                            <br />
                            <small>JUN/2017 at MAR/2018</small>
                        </blockquote>
                        <blockquote>
                            <p>Web Factory Brasil</p>
                            <b>Fullstack<b>
                            <br />
                            <small>MAR/2012 at APR/2017</small>
                        </blockquote>
                        <p>much more...</p>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <p>Hi, my name is Rudy Jordache, Brazilian, currently <?php echo intval(date('Y')) - 1989;?> years old and addicted to technology since I was a child. My first contact with computing was in 1994 with an IBM Aptiva PC from my father. From there to here i have been consuming everything related, especially in what concerns the programming. I graduated high school in 2005 at age 16, when I was able to immerse myself completely in IT. Later I entered the law school of my city, but I had to choose to finish the course or to undertake with a business of my own, which there are many cases like mine, I am still in search of my dreams, and I have confidence that I can reach it , With almost 10 years of IT experience (professionally), I just need a good opportunity to prove my potential. The limits are beyond the sky</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="parallax-container">
      <div class="parallax"><img src="innozone-files/a.jpeg"><div class="pano"></div></div>
    </div>

    <div id="skills" class="container scrollspy">
        <div class="row">
            <div class="col s12">
                <h2 class="title">Skills</h2>
            </div>
            <div class="col s12 m6">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                       <blockquote>Rapid learning (almost surreal)</blockquote> 
                       <blockquote>PHP (all versions)</blockquote>
                       <blockquote>Laravel - Lumen</blockquote>
                       <blockquote>Html5</blockquote>
                       <blockquote>CSS3 - SCSS - SASS - LESS</blockquote>
                       <blockquote>Javascript - Angular2+ - Angular.JS - Typescript - jQuery</blockquote> 
                       <blockquote>API Rest - XML - JSON - Webservice (nuSOAP)</blockquote>
                       <blockquote>MVC - PDO - MySql - MariaDB - MsSql - noSQL</blockquote>
                       <blockquote>Git</blockquote>
                       <blockquote>AWS - Linux (server)</blockquote>
                    </div>
                </div>
            </div>
            <div class="col s12 m6">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                       <blockquote>SEO</blockquote>
                       <blockquote>SCRUM</blockquote>
                       <blockquote>Project management</blockquote>
                       <blockquote>Strong leadership</blockquote>
                       <blockquote>Marketing</blockquote>
                       <blockquote>Legal knowledge (Brazilian)</blockquote>
                       <blockquote>Negotiation</blockquote>
                       <blockquote>Entrepreneurship feeling</blockquote>
                       <blockquote>Coreldraw - Photoshop - Lightroom - Illustrator</blockquote>
                       <blockquote>Sonyvegas - Sony Sound Forge - Edge Animate - After Effects</blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="parallax-container">
      <div class="parallax"><img src="innozone-files/e.jpg"><div class="pano"></div></div>
    </div>

    <div id="contacts" class="container scrollspy">
        <div class="row">
            <div class="col s12">
                <h2 class="title">Contacts</h2>
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        
                        <p class="flow-text">Contact me, I would love to talk to you</p>
                        <p>&nbsp;</p>
                        <p class="pack-btns">
                            <a class="waves-effect waves-teal btn" href="https://www.linkedin.com/in/rjordache/" target="_blank">LinkedIn</a>
                            <a class="waves-effect waves-teal btn" href="https://www.facebook.com/rudypaesdeandrade" target="_blank">Facebook</a>
                            <a class="waves-effect waves-teal btn" href="https://www.instagram.com/rudyjordache/" target="_blank">Instagram</a>
                            <a class="waves-effect waves-teal btn" href="mailto:rjordache.dev@gmail.com" target="_blank">E-Mail</a>
                            <a class="waves-effect waves-teal btn" href="https://wa.me/+5581996156081" target="_blank">Whatsapp</a>
                            <a class="waves-effect waves-teal btn tg" href="#" target="_blank">Telegram</a>
                            <a class="waves-effect waves-teal btn print">Print</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </main>
    <footer class="page-footer blue-grey darken-3">
        <div class="footer-copyright">
            <div class="container">
            © <?php date('Y');?> Copyright - Todos os direitos reservados
        </div>
    </footer>
</body>
</html>